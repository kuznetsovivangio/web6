function counter(listValue) {
    var counts = document.getElementsByClassName("counts");
    var count = document.getElementsByName("count");
    var properties = document.getElementById("properties");
    var options = document.getElementById("options");
    var selectedInputs;
    var markup = 0;
    var result2 = document.getElementById("result2");
    var r;
    switch (listValue) {
    case "1":
        counts[0].style.display = "block";
        counts[1].style.display = "none";
        counts[2].style.display = "none";
        properties.style.display = "none";
        options.style.display = "none";
        r = count[0].value * 20;
        result2.innerHTML = (
            r.toString().match(/^[0-9]+$/) === null
            ? "Не пытайтесь меня обмануть!"
            : "Cтоимость зелий равна " + r + " зол."
        );
        break;
    case "2":
        counts[0].style.display = "none";
        counts[1].style.display = "block";
        counts[2].style.display = "none";
        properties.style.display = "block";
        options.style.display = "none";
        selectedInputs = document.querySelectorAll("input[name=property]");
        selectedInputs.forEach(function (item) {
            if (item.checked) {
                markup += parseInt(item.value);
            }
        });
        r = count[1].value * (100 + markup);
        result2.innerHTML = (
            r.toString().match(/^[0-9]+$/) === null
            ? "Не пытайтесь меня обмануть!"
            : "Cтоимость мечей равна " + r + " зол."
        );
        break;
    case "3":
        counts[0].style.display = "none";
        counts[1].style.display = "none";
        counts[2].style.display = "block";
        properties.style.display = "none";
        options.style.display = "block";
        selectedInputs = document.querySelectorAll("input[name=option]");
        selectedInputs.forEach(function (item) {
            if (item.checked) {
                markup += parseInt(item.value);
            }
        });
        r = count[2].value * (80 + markup);
        result2.innerHTML = (
            r.toString().match(/^[0-9]+$/) === null
            ? "Не пытайтесь меня обмануть!"
            : "Cтоимость луков равна " + r + " зол."
        );
        break;
    }
}

document.addEventListener("DOMContentLoaded", function () {
    var list = document.getElementsByName("list");
    counter(list[0].value);
    list[0].addEventListener("change", function () {
        counter(list[0].value);
    });
    [].forEach.call(document.getElementsByTagName("input"), function (item) {
        item.addEventListener("change", function () {
            counter(list[0].value);
        });
    });
});

function click1() {
    var f1 = document.getElementsByName("field1");
    var f2 = document.getElementsByName("field2");
    var r = document.getElementById("result1");
    var x = f1[0].value * f2[0].value;

    r.innerHTML = (
        x.toString().match(/^[0-9]+$/) === null
        ? "Некорректный ввод"
        : "Общая стоимость равна " + x + " руб."
    );

    return false;
}
